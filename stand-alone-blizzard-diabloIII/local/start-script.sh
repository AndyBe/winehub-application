#!/bin/bash
LAUNCHER="$WINEPREFIX/drive_c/Program Files/Battle.net/Battle.net Launcher.exe"
if [ -f "$LAUNCHER" ]; then
    export  WINEDEBUG=-all
	wine "$LAUNCHER"
else
    /bin/bash
fi
