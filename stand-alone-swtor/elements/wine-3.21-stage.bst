kind: autotools

depends:
- filename: wine-extensions.bst
- filename: base/buildsystem-autotools.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: base/bison.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: base/patch.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: base-platform.bst
  junction: freedesktop-sdk.bst
  type: build 
- filename: desktop/ffmpeg.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/fontconfig.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/freetype.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/libpulse.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/libva.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/libvdpau.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/mesa.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/mpg123.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/openal.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/opencl.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/vulkan.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/xorg-lib-xcursor.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/xorg-lib-xinerama.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: desktop/xorg-lib-xcomposite.bst
  junction: freedesktop-sdk.bst
  type: build
variables:
  prefix: "%{wine_prefix}"
  bindir: "%{wine_bindir}"
  sbindir: "%{wine_sbindir}"
  libdir: "%{wine_libdir}"
  (?):
   - target_arch == "x86_64":
       wine_extra_flags: "--enable-win64 --disable-win16"
   - target_arch == "i686":
       wine_extra_flags: "--disable-win16"

  conf-local: |
    CFLAGS="$CFLAGS -I%{wine_prefix}/include" \
    CXXFLAGS="$CXXFLAGS -I%{wine_prefix}/include" \
    LDFLAGS="$LDFLAGS -L/usr/lib/%{gcc_arch}-gnu-linux -L%{libdir}" \
     %{wine_extra_flags} \
     --with-d3d9-nine \
     --with-d3d9-nine-module=/usr/lib/%{gcc_arch}-linux-gnu/d3d/d3dadapter9.so.1 \
     --without-cups \
     --without-curses \
     --without-gphoto \
     --without-gstreamer \
     --without-hal \
     --without-oss \
     --without-capi \
     --without-pcap \
     --without-sane \
     --without-sdl \
     --without-udev \
     --without-v4l \
     --disable-winemenubuilder \
     --disable-tests

config:
  configure-commands:
    - ./patches/patchinstall.sh --all
    - patch -Np1 -i ./staging-helper.patch
    - patch -Np1 -i ./wine-d3d9.patch
    - autoreconf -v -f -i
    - ./%{configure}

  build-commands:
    - make -j$(nproc)

  install-commands:
    - make install DESTDIR=%{install-root}

sources:
- kind: tar
  url: https://dl.winehq.org/wine/source/3.x/wine-3.21.tar.xz
  ref: a84cc06015df06e12c524213a98d879caa0d63f474b911cdd87f693fcfe2e0c0

- kind: tar
  url: https://github.com/wine-staging/wine-staging/archive/v3.21.tar.gz
  ref: 797c45022238e839a8553f5d7593df31b2cc655120f4c820560c33d05827557b

- kind: tar
  url: https://github.com/sarnex/wine-d3d9-patches/archive/wine-d3d9-3.21.tar.gz
  ref: 951e2de22d6efc739d05e2b884c67c0bab93490615deae90c5b346595f0cbd06

- kind: local
  path: patches/wine-3.19-steam.patch


