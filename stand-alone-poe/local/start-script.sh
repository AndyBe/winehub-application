#!/bin/bash
PRG_PATH="$WINEPREFIX/drive_c/Program Files/Grinding Gear Games/Path of Exile"
LAUNCHER="PathOfExile.exe"
if [ -d "$PRG_PATH" ]; then
    export  WINEDEBUG=-all
    # we must start inside the programm path, 
    # otherwise data will be lost for each restart.
    cd "$PRG_PATH"
	wine "$LAUNCHER"
else
    /bin/bash
fi
